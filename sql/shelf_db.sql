-- phpMyAdmin SQL Dump
-- version 4.2.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2016 年 6 月 15 日 10:26
-- サーバのバージョン： 5.5.40
-- PHP Version: 5.4.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DROP TABLE IF EXISTS `modx_slf_products`;
CREATE TABLE IF NOT EXISTS `modx_slf_products` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `shelf_display_from`  int(11) DEFAULT NULL,
  `shelf_display_to`  int(11) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `page_id`  int(11) DEFAULT NULL,
  `discontinued_flg` tinyint(4) DEFAULT 0,
  `dosage_form` varchar(255) NOT NULL,
  `web_category`  int(11) NOT NULL,
  `product_grp_name` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `capacity` varchar(255) DEFAULT NULL,
  `main_flg` tinyint(4) DEFAULT 0,
  `display_order`  int(11) DEFAULT 0,
  `shop_url` varchar(255) DEFAULT NULL,
  `efficacious_plant` varchar(1000) DEFAULT NULL,
  `efficacy` varchar(1000) DEFAULT NULL,
  `registered_chemical` varchar(255) DEFAULT NULL,
  `component` varchar(255) DEFAULT NULL,
  `characteristic` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `instruction` varchar(1000) DEFAULT NULL,
  `trademark` varchar(255) DEFAULT NULL,
  `trademark_show_flg` tinyint(4) DEFAULT 0,
  `feature` mediumtext DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `discontinued_date` varchar(255) DEFAULT NULL,
  `workability` varchar(1000) DEFAULT NULL,
  `herbicide_purpose` varchar(1000) DEFAULT NULL,
  `herbicide_type` varchar(255) DEFAULT NULL,
  `fertilizer_target` varchar(1000) DEFAULT NULL,
  `fertilizer_way` varchar(255) DEFAULT NULL,
  `fertilizer_for` varchar(255) DEFAULT NULL,
  `fertilizer_effect` varchar(255) DEFAULT NULL,
  `fertilizer_effective_duration` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_products`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_products`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_product_categories`;
CREATE TABLE IF NOT EXISTS `modx_slf_product_categories` (
`id` int(11) NOT NULL,
  `page_id`  int(11) NOT NULL,
  `product_grp_name` varchar(255) DEFAULT NULL,
  `1st_category`  int(11) NOT NULL,
  `2nd_category`  int(11) NOT NULL,
  `discontinued_flg` tinyint(4) DEFAULT 0,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_product_categories`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_product_categories`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_publish_dates`;
CREATE TABLE IF NOT EXISTS `modx_slf_publish_dates` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `table_infomation`  int(11) NOT NULL,
  `publish_date`  int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_publish_dates`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_publish_dates`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_diseases`;
CREATE TABLE IF NOT EXISTS `modx_slf_diseases` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `shelf_display_from`  int(11) DEFAULT NULL,
  `shelf_display_to`  int(11) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `page_id`  int(11) DEFAULT NULL,
  `syllabary` int(11) NOT NULL,
  `disease_name` varchar(255) DEFAULT NULL,
  `disease_name_kana` varchar(255) DEFAULT NULL,
  `summary` mediumtext DEFAULT NULL,
  `emergence_period` varchar(1000) DEFAULT NULL,
  `emergence_target` varchar(1000) DEFAULT NULL,
  `disease_type` varchar(1000) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_diseases`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_diseases`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_pests`;
CREATE TABLE IF NOT EXISTS `modx_slf_pests` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `shelf_display_from`  int(11) DEFAULT NULL,
  `shelf_display_to`  int(11) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `page_id`  int(11) DEFAULT NULL,
  `syllabary` int(11) NOT NULL,
  `pest_name` varchar(255) DEFAULT NULL,
  `pest_name_kana` varchar(255) DEFAULT NULL,
  `summary` mediumtext DEFAULT NULL,
  `emergence_period` varchar(1000) DEFAULT NULL,
  `emergence_target` varchar(1000) DEFAULT NULL,
  `pest_type` varchar(1000) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_pests`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_pests`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_unpleasant_pests`;
CREATE TABLE IF NOT EXISTS `modx_slf_unpleasant_pests` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `shelf_display_from`  int(11) DEFAULT NULL,
  `shelf_display_to`  int(11) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `page_id`  int(11) DEFAULT NULL,
  `syllabary` int(11) NOT NULL,
  `unpleasant_pest_name` varchar(255) DEFAULT NULL,
  `unpleasant_pest_name_kana` varchar(255) DEFAULT NULL,
  `summary` mediumtext DEFAULT NULL,
  `emergence_period` varchar(1000) DEFAULT NULL,
  `emergence_target` varchar(1000) DEFAULT NULL,
  `pest_type` varchar(1000) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_unpleasant_pests`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_unpleasant_pests`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_plants`;
CREATE TABLE IF NOT EXISTS `modx_slf_plants` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `shelf_display_from`  int(11) DEFAULT NULL,
  `shelf_display_to`  int(11) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `page_id`  int(11) DEFAULT NULL,
  `navi_flg` int(11) NOT NULL,
  `large_group` varchar(255) NOT NULL,
  `large_group_code` varchar(50) NOT NULL,
  `middle_group` varchar(255) NOT NULL,
  `middle_group_code` varchar(50) NOT NULL,
  `syllabary` varchar(255) NOT NULL,
  `plant_name` varchar(255) DEFAULT NULL,
  `plant_name_kana` varchar(255) DEFAULT NULL,
  `formal_plant_name` varchar(255) DEFAULT NULL,
  `planting _in` varchar(255) DEFAULT NULL,
  `summary` mediumtext DEFAULT NULL,
  `plant_family_name` varchar(255) DEFAULT NULL,
  `original_home` varchar(255) DEFAULT NULL,
  `gardening_group` varchar(255) DEFAULT NULL,
  `plant_group` varchar(255) DEFAULT NULL,
  `vegetable_group` varchar(255) DEFAULT NULL,
  `at_beginning` varchar(255) DEFAULT NULL,
  `sunlight_condition` varchar(255) DEFAULT NULL,
  `suitable_temperature` varchar(255) DEFAULT NULL,
  `watering` mediumtext DEFAULT NULL,
  `feature` mediumtext DEFAULT NULL,
  `plant_height` varchar(255) DEFAULT NULL,
  `plant_period_search` varchar(255) DEFAULT NULL,
  `plant_period_display` varchar(255) DEFAULT NULL,
  `plant_seed_period_search` varchar(255) DEFAULT NULL,
  `plant_seed_period_display` varchar(255) DEFAULT NULL,
  `flowering_period_search` varchar(255) DEFAULT NULL,
  `flowering_period_display` varchar(255) DEFAULT NULL,
  `harvest_period_search` varchar(255) DEFAULT NULL,
  `harvest_period_display` varchar(255) DEFAULT NULL,
  `period_of_growth` varchar(255) DEFAULT NULL,
  `period_after_frowering` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_plants`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_plants`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_plants_search`;
CREATE TABLE IF NOT EXISTS `modx_slf_plants_search` (
`id` int(11) NOT NULL,
  `relation_id` varchar(255) NOT NULL,
  `shelf_display_from`  int(11) DEFAULT NULL,
  `shelf_display_to`  int(11) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `page_id`  int(11) DEFAULT NULL,
  `large_group` varchar(255) DEFAULT NULL,
  `large_group_code` varchar(50) DEFAULT NULL,
  `middle_group` varchar(255) DEFAULT NULL,
  `middle_group_code` varchar(50) DEFAULT NULL,
  `syllabary` varchar(255) NOT NULL,
  `plant_name` varchar(255) DEFAULT NULL,
  `plant_name_kana` varchar(255) DEFAULT NULL,
  `plant_family_name` varchar(255) DEFAULT NULL,
  `gardening_group` varchar(255) DEFAULT NULL,
  `plant_group` varchar(255) DEFAULT NULL,
  `vegetable_group` varchar(255) DEFAULT NULL,
  `plant_period_search` varchar(255) DEFAULT NULL,
  `plant_seed_period_search` varchar(255) DEFAULT NULL,
  `flowering_period_search` varchar(255) DEFAULT NULL,
  `harvest_period_search` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_plants_search`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_plants_search`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_weeds`;
CREATE TABLE IF NOT EXISTS `modx_slf_weeds` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `shelf_display_from`  int(11) DEFAULT NULL,
  `shelf_display_to`  int(11) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `page_id`  int(11) DEFAULT NULL,
  `syllabary` varchar(255) NOT NULL,
  `weed_name` varchar(255) DEFAULT NULL,
  `weed_name_kana` varchar(255) DEFAULT NULL,
  `weed_group` varchar(255) DEFAULT NULL,
  `breeding` varchar(255) DEFAULT NULL,
  `flowering_period` varchar(255) DEFAULT NULL,
  `classification_name` varchar(255) DEFAULT NULL,
  `weed_height` varchar(255) DEFAULT NULL,
  `distribution` varchar(255) DEFAULT NULL,
  `period_of_growth` varchar(255) DEFAULT NULL,
  `botany` mediumtext DEFAULT NULL,
  `behind_the_name` mediumtext DEFAULT NULL,
  `anotner_name` varchar(255) DEFAULT NULL,
  `pollinosis_flg` tinyint(4) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_weeds`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_weeds`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_file_info`;
CREATE TABLE IF NOT EXISTS `modx_slf_file_info` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
`table_infomation` int(11) NOT NULL,
`file_name` varchar(255) DEFAULT NULL,
`title1` varchar(255) DEFAULT NULL,
`title2` varchar(255) DEFAULT NULL,
`title3` varchar(255) DEFAULT NULL,
`file_type` int(11) DEFAULT NULL,
`display_order` int(11) DEFAULT NULL,
`file_id` varchar(255) DEFAULT NULL,
`main_image_flag` int(11) DEFAULT NULL,
`image_status` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_file_info`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_file_info`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_file_info_history`;
CREATE TABLE IF NOT EXISTS `modx_slf_file_info_history` (
`id` int(11) NOT NULL,
`relation_id` varchar(255) NOT NULL,
  `table_infomation`  int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `title1` varchar(255) DEFAULT NULL,
  `title2` varchar(255) DEFAULT NULL,
  `title3` varchar(255) DEFAULT NULL,
  `file_type`  int(11) DEFAULT NULL,
  `display_order`  int(11) DEFAULT NULL,
  `shelf_file_id` varchar(255) DEFAULT NULL,
  `shelf_updated_at`  int(11) DEFAULT NULL,
  `main_image_flag`  int(11) DEFAULT NULL,
  `image_status`  int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_file_info_history`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_file_info_history`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_efficacious_plants_pests_diseases`;
CREATE TABLE IF NOT EXISTS `modx_slf_efficacious_plants_pests_diseases` (
`id` int(11) NOT NULL,
  `plant_large_group_name` varchar(255) DEFAULT NULL,
  `plant_large_group_code` varchar(50) DEFAULT NULL,
  `plant_middle_group_name` varchar(255) DEFAULT NULL,
  `plant_middle_group_code` varchar(50) DEFAULT NULL,
  `plant_name` varchar(255) DEFAULT NULL,
  `plant_relation_id` varchar(50) DEFAULT NULL,
  `exclude_plant_middle_group_name` varchar(255) DEFAULT NULL,
  `exclude_plant_middle_group_code` varchar(50) DEFAULT NULL,
  `exclude_plant_name` varchar(255) DEFAULT NULL,
  `exclude_plant_relation_id` varchar(50) DEFAULT NULL,
  `pest_disease_name` varchar(255) DEFAULT NULL,
  `pest_disease_relation_id` varchar(50) DEFAULT NULL,
  `table_infomation` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_relation_id` varchar(50) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_efficacious_plants_pests_diseases`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_efficacious_plants_pests_diseases`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_efficacious_unpleasant_pests`;
CREATE TABLE IF NOT EXISTS `modx_slf_efficacious_unpleasant_pests` (
`id` int(11) NOT NULL,
  `pest_name` varchar(255) DEFAULT NULL,
  `pest_relation_id`  int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_relation_id`  int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_efficacious_unpleasant_pests`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_efficacious_unpleasant_pests`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_efficacious_weeds`;
CREATE TABLE IF NOT EXISTS `modx_slf_efficacious_weeds` (
`id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_relation_id`  int(11) DEFAULT NULL,
  `weed_group` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_efficacious_weeds`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_efficacious_weeds`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_links_to`;
CREATE TABLE IF NOT EXISTS `modx_slf_links_to` (
`id` int(11) NOT NULL,
  `relation_id`  int(11) NOT NULL,
  `table_infomation`  int(11) NOT NULL,
  `page_id`  int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_links_to`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_links_to`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `modx_slf_link_keywords`;
CREATE TABLE IF NOT EXISTS `modx_slf_link_keywords` (
`id` int(11) NOT NULL,
  `relation_id`  int(11) NOT NULL,
  `page_id`  int(11) NOT NULL,
  `link_keyword` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `modx_slf_link_keywords`
 ADD PRIMARY KEY (`id`);

 ALTER TABLE `modx_slf_link_keywords`
 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;







/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
