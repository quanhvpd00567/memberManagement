<?php

namespace Manager;

class Controller_Index extends Controller_Base {

    public function before()
    {
        parent::before();

        $this->template->pagetitle = \Constants::$page_title['login'];
    }

    public function action_login()
    {
        // すでにログインしているかチェック
        \Auth::check() and \Response::redirect('manager/dashboard/index');

        // ログイン用validateルール取得
        $val = \Model_User::validate('MasterLogin');

        if (\Input::method() == 'POST')
        {
            if ($val->run())
            {
                //validate問題なし
                if ( ! \Auth::check()) // すでにログインしているかチェック
                {
                    if (\Auth::login(\Input::post('username'), \Input::post('password'))) //ログインチェック
                    {
                        // 管理者ユーザーか確認
                        $admin_group_id = \Constants::$user_group['Administrators'];
                        if (\Auth::member($admin_group_id)) {
                            //管理者ユーザーならメイン画面へ
                            \Response::redirect('manager/dashboard/index');
                        } else {
                            //管理者ユーザーではないならログアウトしてログイン画面へ
                            \Auth::logout();
                            \Response::redirect('manager/index/login');
                        }
                    }
                    else
                    {
                        // ログインエラーメッセージは設定されたエラーメッセージをセット
                        $this->template->set_global('error', \Constants::$error_message['login_error']);
                    }
                }
                else
                {
                    // ログインエラーメッセージは設定されたエラーメッセージをセット
                    $this->template->set_global('error', \Constants::$error_message['already_logged_in']);
                }
            }
        }

        // ログイン画面を表示
        $this->template->content = \View_Smarty::forge('index/login.tpl', array('val' => $val), false);
    }

    /**
     * The logout action.
     *
     * @access  public
     * @return  void
     */
    public function action_logout() //ログアウトする
    {
        \Auth::logout();
        \Response::redirect('manager/index/login');
    }

    /**
     * The index action.
     *
     * @access  public
     * @return  void
     */
    public function action_index() // すでに管理者ユーザーでログインしているなら、メイン画面へ、そうでないならログイン画面へ
    {
        $admin_group_id = \Constants::$user_group['Administrators'];
        if (\Auth::check() && \Auth::member($admin_group_id)){
            \Response::redirect('manager/dashboard/index');
        } else {
            \Response::redirect('manager/index/login');
        }
    }

}

/* End of file index.php */
