<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>[+$title+]</title>
    <base href="[+Uri::base()+]" />
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <!-- site css -->
    [+Asset::css(array('site.min.css','family.css'))+]
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    [+Asset::js(array('site.min.js'))+]
</head>


<body>

<div class="docs-header">
    <!--nav-->
    <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="manager/dashboard/index"><img src="assets/img/logo.png" height="40"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    [+if $logined_flag == 1 +]
                    <li><a class="nav-link[+if $controller == 'photoedit' +] current[+/if+]" href="manager/photoedit/add">情報登録</a></li>
                    <li><a class="nav-link[+if $controller == 'photolist' +] current[+/if+]" href="manager/photolist/index">登録一覧リスト</a></li>
                    <li><a class="nav-link[+if $controller == 'photomap' +] current[+/if+]" href="manager/photomap/index">登録地図</a></li>
                    <li><a class="nav-link" href="manager/index/logout">ログアウト</a></li>
                    [+else+]
                    <li><a class="nav-link" href="manager/index/login">ログイン</a></li>
                    [+/if+]
                </ul>
            </div>
        </div>
    </nav>
    <!--header-->
</div>


<!--全体-->
<div class="container documents">
    [+$content+]
</div>
<!--全体END-->


<!--フッター-->
<!--footer-->
<div class="site-footer">
    <div class="container">
        <div class="copyright clearfix">
            <p><b>簡易プライベートDMP管理システム</b>&nbsp;&nbsp;&nbsp;&nbsp;&copy; 2017 住友化学園芸. All rights reserved.</p>
        </div>
    </div>
</div>    <!--フッターEND-->
</body>
</html>